package org.mantiscorp.effectiverate.calc;

import lombok.extern.java.Log;
import org.mantiscorp.effectiverate.AppOptions;
import org.mantiscorp.effectiverate.backupfile.BackupFileEntitiesParser;
import org.mantiscorp.effectiverate.business.Account;
import org.mantiscorp.effectiverate.business.Category;
import org.mantiscorp.effectiverate.business.Currency;
import org.mantiscorp.effectiverate.business.Transaction;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Predicate;

@Log
public class Calculator {
    private final String fileName;
    private final Collection<String> accountNames = new ArrayList<>();
    private final BackupFileEntitiesParser backupFileEntitiesParser = new BackupFileEntitiesParser();
    private final BinaryOperator<BigDecimal> divide = (a, b) -> a.divide(b, 8, RoundingMode.HALF_UP);

    public Calculator(AppOptions appOptions) {
        super();

        fileName = appOptions.getInputFileName();
        getAccountNames().addAll(appOptions.getAccountNames());
    }

    public void calculate() throws IOException {
        final Map<Account, Collection<Transaction>> accountTransactions = new TreeMap<>(Comparator.comparing(Account::getCreationDate));
        final BiConsumer<Account, Transaction> accountTransactionsCollector = (a, t) -> accountTransactions.computeIfAbsent(a, acc -> new ArrayList<>()).add(t);
        backupFileEntitiesParser.parseBackupFile(fileName).forEach(t -> {
            t.getFromAccount().ifPresent(a -> accountTransactionsCollector.accept(a, t));
            t.getToAccount().ifPresent(a -> accountTransactionsCollector.accept(a, t));
        });
        if (getAccountNames().isEmpty()) { // get all BANK BYN accounts
            final Currency byn = backupFileEntitiesParser.getCurrencies().stream().filter(currency -> "BYN".equals(currency.getName())).findAny().orElseThrow(IllegalStateException::new);
            final Date lastTransactionThreshold = getLastTransactionThreshold();
            final Predicate<Account> accountFilter = account -> account.getCurrency().equals(byn) && "BANK".equals(account.getAccountType()) && account.getLastTransactionDate().after(lastTransactionThreshold);
            accountTransactions.keySet().stream().filter(accountFilter).map(Account::getTitle).forEach(getAccountNames()::add);
        }
        final Predicate<Map.Entry<Account, Collection<Transaction>>> accountFilter = entry -> getAccountNames().contains(entry.getKey().getTitle());
        var currency = backupFileEntitiesParser.getCurrencies().stream().filter(Currency::isDefault).findFirst().orElseThrow(IllegalStateException::new);
        accountTransactions.entrySet().stream().filter(accountFilter).forEach(e ->
                calcEffectiveRate(e.getKey(), e.getValue(), currency)
        );
    }

    private static Date getLastTransactionThreshold() {
        var calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, -1);
        return calendar.getTime();
    }

    private Collection<String> getAccountNames() {
        return accountNames;
    }

    private void calcEffectiveRate(Account account, Collection<Transaction> transactions, Currency currency) {
        final Comparator<Date> dateComparator = Date::compareTo;
        var minDate = transactions.stream().map(Transaction::getDatetime).min(dateComparator).orElseThrow(IllegalStateException::new);
        var maxDate = transactions.stream().map(Transaction::getDatetime).max(dateComparator).orElseThrow(IllegalStateException::new);

        var dateFormat = DateFormat.getDateInstance(DateFormat.SHORT);
        log.info(account.getTitle() + " (" + dateFormat.format(minDate) + " - " + dateFormat.format(maxDate) + ")");

        final BigDecimal balance = transactions.stream().map(
                t -> {
                    if (t.getToAccount().filter(account::equals).isPresent())
                        return t.getToAmount();
                    else if (t.getFromAccount().filter(account::equals).isPresent())
                        return t.getFromAmount();
                    else return BigDecimal.ZERO;
                }
        ).reduce(BigDecimal.ZERO.setScale(account.getCurrency().getDecimals(), RoundingMode.HALF_UP), BigDecimal::add);
        final BigDecimal outBalance = Transaction.getAmount(balance, account.getCurrency(), currency, new Date());

        final BigDecimal zero = BigDecimal.ZERO.setScale(currency.getDecimals(), RoundingMode.HALF_UP);
        // account in
        final BigDecimal in0 = transactions.stream().filter(
                t -> t.getToAccount().filter(account::equals).isPresent()
        ).map(t -> t.getToAmount(currency)).reduce(zero, BigDecimal::add);
        // exchanges
        final BigDecimal in1 =  transactions.stream().filter(
                t -> t.getFromAccount().filter(account::equals).isPresent() && !t.getCategory().map(Category::isIncome).orElse(true) && t.getFromAmount().signum() > 0
        ).map(t -> t.getFromAmount(currency)).reduce(zero, BigDecimal::add);
        // account out
        final BigDecimal out0 = transactions.stream().filter(
                t -> t.getFromAccount().filter(account::equals).isPresent() && t.getFromAmount().signum() < 0 && !t.getCategory().map(Category::isIncome).orElse(Boolean.FALSE)
        ).map(t -> t.getFromAmount(currency)).reduce(zero, BigDecimal::subtract);

        final BigDecimal in = in0.add(in1);
        final BigDecimal out = out0.add(outBalance);

        log.info("In: " + in + currency.getSymbol());
        log.info("Out: " + out + currency.getSymbol());
        if (balance.signum() != 0)
            log.info("Balance: " + balance + ' ' + account.getCurrency().getSymbol());

        final long days = TimeUnit.DAYS.convert(maxDate.getTime() - minDate.getTime(), TimeUnit.MILLISECONDS);

        final BigDecimal result = in.signum() == 0 || days == 0 ? BigDecimal.ZERO : divide.apply(
                divide.apply(out.subtract(in), in),
                divide.apply(
                        BigDecimal.valueOf(days),
                        BigDecimal.valueOf(365)
                )
        ).multiply(BigDecimal.valueOf(100)).setScale(1, RoundingMode.HALF_UP);
        log.info("Effective rate: " + result + '%');
        log.info("");
    }
}
