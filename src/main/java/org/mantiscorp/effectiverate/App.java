package org.mantiscorp.effectiverate;

import lombok.NoArgsConstructor;
import lombok.extern.java.Log;
import org.apache.commons.cli.ParseException;
import org.mantiscorp.effectiverate.calc.Calculator;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.LogManager;

/**
 *
 */
@Log
@NoArgsConstructor
public class App {

    public static void main(String[] args) {
        try {
            LogManager.getLogManager().readConfiguration(
                    App.class.getClassLoader().getResourceAsStream("logging.properties")
            );

            final App app = new App();
            app.run(args);

            log.info("SUCCESS");
            System.exit(0);
        } catch (ParseException e) {
            System.exit(1); // ParseException handled elsewhere
        } catch (Exception e) {
            log.log(Level.SEVERE, "main", e);
            System.exit(1);
        }
    }

    public void run(String[] args) throws IOException, ParseException {
        new Calculator(new AppOptions(args)).calculate();
    }
}
