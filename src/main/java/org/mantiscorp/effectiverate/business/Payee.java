package org.mantiscorp.effectiverate.business;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 */
@NoArgsConstructor
public class Payee extends Entity {
    @Getter
    @Setter
    private String title;

    @Override
    public String toString() {
        return getTitle();
    }
}
