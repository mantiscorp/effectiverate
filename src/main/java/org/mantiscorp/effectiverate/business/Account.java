package org.mantiscorp.effectiverate.business;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 */
@NoArgsConstructor
public class Account extends Entity {
    @Getter
    @Setter
    private String title;
    @Getter
    @Setter
    private Currency currency;
    @Getter
    @Setter
    private long sortOrder;
    @Getter
    @Setter
    private boolean isActive;
    @Getter
    @Setter
    private BigDecimal totalLimit;
    @Getter
    @Setter
    private String accountType;
    @Getter
    @Setter
    private Date lastTransactionDate;
    @Getter
    @Setter
    private Date creationDate;

    @Override
    public String toString() {
        return getTitle();
    }
}
