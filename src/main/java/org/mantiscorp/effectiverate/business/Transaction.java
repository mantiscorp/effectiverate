package org.mantiscorp.effectiverate.business;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;

/**
 *
 */
@NoArgsConstructor
public class Transaction extends Entity {
    @Setter
    private Account fromAccount;
    @Setter
    private Account toAccount;
    @Setter
    private Category category;
    @Setter
    private Project project;
    @Getter
    @Setter
    private BigDecimal fromAmount;
    @Getter
    @Setter
    private BigDecimal toAmount;
    @Getter
    @Setter
    private Date datetime;
    @Setter
    private Payee payee;
    @Getter
    @Setter
    private Transaction parentId;
    @Getter
    @Setter
    private boolean isTemplate;

    public Optional<Account> getFromAccount() {
        return Optional.ofNullable(fromAccount);
    }

    public Optional<Account> getToAccount() {
        return Optional.ofNullable(toAccount);
    }

    public Optional<Category> getCategory() {
        return Optional.ofNullable(category);
    }

    public Optional<Project> getProject() {
        return Optional.ofNullable(project);
    }

    public Optional<Payee> getPayee() {
        return Optional.ofNullable(payee);
    }

    public BigDecimal getFromAmount(Currency toCurrency) {
        return getAmount(this.fromAmount, getFromAccount().map(Account::getCurrency).orElseThrow(IllegalStateException::new), toCurrency);
    }

    public BigDecimal getToAmount(Currency toCurrency) {
        return getAmount(this.toAmount, getToAccount().map(Account::getCurrency).orElseThrow(IllegalStateException::new), toCurrency);
    }

    private BigDecimal getAmount(BigDecimal amount, Currency fromCurrency, Currency toCurrency) {
        return getAmount(amount, fromCurrency, toCurrency, getDatetime());
    }

    public static BigDecimal getAmount(BigDecimal amount, Currency fromCurrency, Currency toCurrency, Date date) {
        if (amount.signum() == 0 || fromCurrency.equals(toCurrency))
            return amount;
        else {
            final Optional<ExchangeRate> rate = toCurrency.getExchangeRates().stream()
                    .filter(er -> er.getToCurrency().equals(fromCurrency))
                    .filter(er -> er.getDate().compareTo(date) <= 0)
                    .max(Comparator.comparing(ExchangeRate::getDate));
            return rate.map(exchangeRate -> amount.divide(exchangeRate.getRate(), RoundingMode.HALF_UP)).orElseGet(toCurrency::getZero);
        }
    }

    @Override
    public String toString() {
        return DateFormat.getDateInstance(DateFormat.MEDIUM).format(getDatetime()) + ' ' + getFromAmount() + ' '
                + getFromAccount().map(Account::getCurrency).map(Currency::getName).orElse("?") + " -> "
                + (getCategory().map(Objects::toString).orElse(getToAccount().map(Account::getTitle).orElse("?")));
    }
}
