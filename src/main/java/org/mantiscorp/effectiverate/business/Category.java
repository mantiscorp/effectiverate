package org.mantiscorp.effectiverate.business;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 */
@NoArgsConstructor
public class Category extends Entity {
    @Getter
    @Setter
    private String title;
    @Getter
    @Setter
    private long left;
    @Getter
    @Setter
    private long right;
    @Getter
    @Setter
    private long sortOrder;
    @Getter
    @Setter
    private boolean isIncome;
    @Getter
    private final Collection<Category> children = new ArrayList<>();
    public static final Category SPLIT = new Category(-1, "Split");

    public Category(int id, String title) {
        this();

        setId(id);
        setTitle(title);
    }

    public void setParent(Category parent) {
        parent.getChildren().add(this);
    }

    @Override
    public String toString() {
        return getTitle();
    }
}
