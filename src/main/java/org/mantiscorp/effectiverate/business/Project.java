package org.mantiscorp.effectiverate.business;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 */
@NoArgsConstructor
public class Project extends Entity {
    @Getter
    @Setter
    private String title;
    // is_active:1

    @Override
    public String toString() {
        return getTitle();
    }
}
