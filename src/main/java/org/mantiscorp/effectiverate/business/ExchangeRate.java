package org.mantiscorp.effectiverate.business;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.Date;

/**
 *
 */
@NoArgsConstructor
public class ExchangeRate implements Comparable<ExchangeRate> {
    @Getter
    @Setter
    private Date date;
    @Getter
    @Setter
    private BigDecimal rate;
    @Getter
    @Setter
    private Currency fromCurrency;
    @Getter
    @Setter
    private Currency toCurrency;

    @Override
    public int compareTo(ExchangeRate o) {
        return getDate().compareTo(o.getDate());
    }

    @Override
    public boolean equals(Object obj){
        return super.equals(obj);
    }

    @Override
    public int hashCode(){
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "1 " + getFromCurrency() + " = " + getRate() + ' ' + getToCurrency() + " at "
                + DateFormat.getDateInstance(DateFormat.SHORT).format(getDate());
    }
}
