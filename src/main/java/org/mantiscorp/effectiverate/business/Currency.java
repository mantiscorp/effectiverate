package org.mantiscorp.effectiverate.business;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 */
@NoArgsConstructor
public class Currency extends Entity {
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private String symbol;
    @Getter
    @Setter
    private boolean isDefault;
    @Getter
    @Setter
    private int decimals;
    @Getter
    private final Collection<ExchangeRate> exchangeRates = new ArrayList<>();

    public BigDecimal getZero() {
        return BigDecimal.ZERO.setScale(getDecimals(), RoundingMode.HALF_UP);
    }

    @Override
    public String toString() {
        return getName();
    }
}
