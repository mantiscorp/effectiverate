package org.mantiscorp.effectiverate.business;

import lombok.Getter;
import lombok.Setter;

/**
 *
 */
public class Entity {
    @Getter
    @Setter
    private int id;

    @Override
    public int hashCode() {
        return getId();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof final Entity entity) {
            return getId() == entity.getId() && getClass().equals(entity.getClass());
        } else
            return false;
    }
}
