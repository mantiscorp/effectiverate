package org.mantiscorp.effectiverate;

import lombok.Getter;
import org.apache.commons.cli.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 *
 */
public class AppOptions {
    @Getter
    private final String inputFileName;
    @Getter
    private final Collection<String> accountNames = new ArrayList<>();

    public AppOptions(String[] args) throws ParseException {
        final Options options = new Options();

        final String optInputName = "input";
        final Option optInput = new Option("i", optInputName, true, "input file name");
        optInput.setRequired(true);
        options.addOption(optInput);

        final String optAccountsName = "accounts";
        final Option optAccounts = new Option("a", optAccountsName, true, "account name(s)");
        optAccounts.setRequired(false);
        optAccounts.setArgs(Option.UNLIMITED_VALUES);
        options.addOption(optAccounts);

        final CommandLineParser parser = new DefaultParser();
        final HelpFormatter formatter = new HelpFormatter();
        try {
            final CommandLine cmd = parser.parse(options, args);
            this.inputFileName = cmd.getOptionValue(optInputName);
            getOptionalOptionValues(cmd, optAccountsName, accountNames::add);
        } catch (ParseException e) {
            formatter.printHelp("effectiverate", options);
            throw e;
        }
    }

    private static void getOptionalOptionValues(CommandLine cmd, String optionName, Consumer<? super String> consumer) {
        Optional.ofNullable(cmd.getOptionValues(optionName)).stream().flatMap(Stream::of).forEach(consumer);
    }
}
