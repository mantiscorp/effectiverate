package org.mantiscorp.effectiverate.backupfile.entityparser;

import java.util.Collection;
import java.util.Properties;
import java.util.function.Supplier;

/**
 *
 */
abstract class CollectionEntityParser<E> extends EntityParser<E> {
    private final Collection<E> collection;

    protected CollectionEntityParser(Supplier<E> entitySupplier, Collection<E> collection) {
        super(entitySupplier);
        this.collection = collection;
    }

    @Override
    public E apply(Properties properties) {
        final E entity = super.apply(properties);
        collection.add(entity);
        return entity;
    }
}
