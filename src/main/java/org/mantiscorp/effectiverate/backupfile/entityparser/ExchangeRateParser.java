package org.mantiscorp.effectiverate.backupfile.entityparser;

import org.mantiscorp.effectiverate.business.Currency;
import org.mantiscorp.effectiverate.business.ExchangeRate;

import java.util.Map;
import java.util.Properties;

/**
 *
 */
public class ExchangeRateParser extends EntityParser<ExchangeRate> {
    private final Map<Integer, Currency> currencies;

    public ExchangeRateParser(Map<Integer, Currency> currencies) {
        super(ExchangeRate::new);
        this.currencies = currencies;
    }

    @Override
    protected void loadEntity(Properties properties, ExchangeRate entity) {
        entity.setRate(getBigDecimalProperty(properties, "rate").orElseThrow(IllegalStateException::new));
        entity.setDate(getDateProperty(properties, "rate_date").orElseThrow(IllegalStateException::new));

        final Currency fromCurrency = getEntityProperty(properties, "from_currency_id", currencies);
        entity.setFromCurrency(fromCurrency);
        fromCurrency.getExchangeRates().add(entity);

        final Currency toCurrency = getEntityProperty(properties, "to_currency_id", currencies);
        entity.setToCurrency(toCurrency);
    }
}
