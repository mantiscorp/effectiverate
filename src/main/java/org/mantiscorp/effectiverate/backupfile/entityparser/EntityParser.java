package org.mantiscorp.effectiverate.backupfile.entityparser;

import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 *
 */
@RequiredArgsConstructor
public abstract class EntityParser<E> implements Function<Properties, E> {
    private final Supplier<E> entitySupplier;

    @Override
    public E apply(Properties properties) {
        final E entity = entitySupplier.get();
        loadEntity(properties, entity);
        return entity;
    }

    public void reset() {
        // nothing
    }

    protected abstract void loadEntity(Properties properties, E entity);

    static Optional<Integer> getIntProperty(Properties properties, String key) {
        return Optional.ofNullable(properties.getProperty(key)).map(Integer::valueOf);
    }

    static Optional<Long> getLongProperty(Properties properties, String key)  {
        return Optional.ofNullable(properties.getProperty(key)).map(Long::valueOf);
    }

    static Optional<Date> getDateProperty(Properties properties, String key)  {
        return getLongProperty(properties, key).map(Date::new);
    }

    static Optional<Double> getDoubleProperty(Properties properties, String key)  {
        return Optional.ofNullable(properties.getProperty(key)).map(Double::valueOf);
    }

    static Optional<BigDecimal> getBigDecimalProperty(Properties properties, String key)  {
        return getDoubleProperty(properties, key).map(BigDecimal::valueOf);
    }

    static Optional<BigDecimal> getMoneyProperty(Properties properties, String key)  {
        return getLongProperty(properties, key).map(ll -> BigDecimal.valueOf(ll, 2));
    }

    static boolean getBooleanProperty(Properties properties, String key)  {
        return "1".equals(properties.getProperty(key));
    }

    static <E> E getEntityProperty(Properties properties, String key, Map<Integer, ? extends E> map) {
        final int id = getIntProperty(properties, key).orElseThrow(NullPointerException::new);
        // search in cache
        final E entity = map.get(id);
        if (entity == null && id != 0)
            throw new NullPointerException(key + '=' + id);
        else
            return entity;
    }
}
