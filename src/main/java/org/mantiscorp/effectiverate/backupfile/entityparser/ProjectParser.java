package org.mantiscorp.effectiverate.backupfile.entityparser;

import org.mantiscorp.effectiverate.business.Project;

import java.util.Map;
import java.util.Properties;

/**
 *
 */
public class ProjectParser extends CachedEntityParser<Project> {

    public ProjectParser(Map<Integer, Project> cache) {
        super(Project::new, cache);
    }

    @Override
    protected void loadEntity(Properties properties, Project entity) {
        super.loadEntity(properties, entity);
        entity.setTitle(properties.getProperty("title"));
    }
}
