package org.mantiscorp.effectiverate.backupfile.entityparser;

import org.mantiscorp.effectiverate.business.*;

import java.util.Collection;
import java.util.Map;
import java.util.Properties;

/**
 *
 */
public class TransactionParser extends CollectionEntityParser<Transaction> {
    private final Map<Integer, Account> accounts;
    private final Map<Integer, Category> categories;
    private final Map<Integer, Project> projects;
    private final Map<Integer, Payee> payees;

    public TransactionParser(Collection<Transaction> collection, Map<Integer, Account> accounts,
                             Map<Integer, Category> categories, Map<Integer, Project> projects, Map<Integer, Payee> payees) {
        super(Transaction::new, collection);

        this.accounts = accounts;
        this.categories = categories;
        this.projects = projects;
        this.payees = payees;
    }

    @Override
    protected void loadEntity(Properties properties, Transaction entity)  {
        entity.setId(getIntProperty(properties, "_id").orElseThrow(IllegalStateException::new));

        entity.setFromAccount(getEntityProperty(properties, "from_account_id", accounts));
        entity.setToAccount(getEntityProperty(properties, "to_account_id", accounts));
        entity.setCategory(getEntityProperty(properties, "category_id", categories));
        entity.setProject(getEntityProperty(properties, "project_id", projects));
        getMoneyProperty(properties, "from_amount").ifPresent(entity::setFromAmount);
        getMoneyProperty(properties, "to_amount").ifPresent(entity::setToAmount);
        entity.setPayee(getEntityProperty(properties, "payee_id", payees));
        entity.setDatetime(getDateProperty(properties, "datetime").orElseThrow(IllegalStateException::new));
        entity.setTemplate(getBooleanProperty(properties, "is_template"));
    }
}
