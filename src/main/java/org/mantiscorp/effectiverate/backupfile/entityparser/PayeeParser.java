package org.mantiscorp.effectiverate.backupfile.entityparser;

import org.mantiscorp.effectiverate.business.Payee;

import java.util.Map;
import java.util.Properties;

/**
 *
 */
public class PayeeParser extends CachedEntityParser<Payee> {

    public PayeeParser(Map<Integer, Payee> collection) {
        super(Payee::new, collection);
    }

    @Override
    protected void loadEntity(Properties properties, Payee entity) {
        super.loadEntity(properties, entity);
        entity.setTitle(properties.getProperty("title"));
    }
}
