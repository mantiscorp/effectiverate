package org.mantiscorp.effectiverate.backupfile.entityparser;

import org.mantiscorp.effectiverate.business.Account;
import org.mantiscorp.effectiverate.business.Currency;

import java.util.Map;
import java.util.Properties;

/**
 *
 */
public class AccountParser extends CachedEntityParser<Account> {
    private final Map<Integer, Currency> currencies;

    public AccountParser(Map<Integer, Account> collection, Map<Integer, Currency> currencies) {
        super(Account::new, collection);
        this.currencies = currencies;
    }

    @Override
    protected void loadEntity(Properties properties, Account entity) {
        super.loadEntity(properties, entity);

        entity.setTitle(properties.getProperty("title"));
        getIntProperty(properties, "sort_order").ifPresent(entity::setSortOrder);
        entity.setActive(getBooleanProperty(properties, "is_active"));
        getMoneyProperty(properties, "total_limit").ifPresent(entity::setTotalLimit);
        entity.setAccountType(properties.getProperty("type"));
        entity.setCurrency(getEntityProperty(properties, "currency_id", currencies));
        getDateProperty(properties, "last_transaction_date").ifPresent(entity::setLastTransactionDate);
        getDateProperty(properties, "creation_date").ifPresent(entity::setCreationDate);
    }
}
