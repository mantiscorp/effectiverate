package org.mantiscorp.effectiverate.backupfile.entityparser;

import org.mantiscorp.effectiverate.business.Category;

import java.util.Comparator;
import java.util.Map;
import java.util.Properties;

/**
 *
 */
public class CategoryParser extends CachedEntityParser<Category> {

    public CategoryParser(Map<Integer, Category> cache) {
        super(Category::new, cache);
    }

    @Override
    public void reset() {
        super.reset();
        cacheEntity(Category.SPLIT);
    }

    @Override
    protected void loadEntity(Properties properties, Category entity)  {
        super.loadEntity(properties, entity);

        entity.setTitle(properties.getProperty("title"));
        getLongProperty(properties, "left").ifPresent(entity::setLeft);
        getLongProperty(properties, "right").ifPresent(entity::setRight);
        getLongProperty(properties, "sort_order").ifPresent(entity::setSortOrder);
        entity.setIncome(getBooleanProperty(properties, "type"));
    }

    @Override
    protected void cacheEntity(Map<Integer, Category> map, Category entity) {
        // try to set parent
        map.values().stream()
                .filter(c -> c.getLeft() < entity.getLeft() && c.getRight() > entity.getRight()).min(Comparator.comparingLong(a -> a.getRight() - a.getLeft()))
                .ifPresent(entity::setParent);
        super.cacheEntity(map, entity);
    }
}
