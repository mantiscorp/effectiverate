package org.mantiscorp.effectiverate.backupfile.entityparser;

import org.mantiscorp.effectiverate.business.Currency;

import java.util.Map;
import java.util.Properties;

/**
 *
 */
public class CurrencyParser extends CachedEntityParser<Currency> {

    public CurrencyParser(Map<Integer, Currency> cache) {
        super(Currency::new, cache);
    }

    @Override
    protected void loadEntity(Properties properties, Currency entity) {
        super.loadEntity(properties, entity);

        entity.setName(properties.getProperty("name"));
        entity.setSymbol(properties.getProperty("symbol"));
        entity.setDecimals(getIntProperty(properties, "decimals").orElseThrow(IllegalStateException::new));
        entity.setDefault(getBooleanProperty(properties, "is_default"));
    }
}
