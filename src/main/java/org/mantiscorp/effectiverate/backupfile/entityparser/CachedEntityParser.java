package org.mantiscorp.effectiverate.backupfile.entityparser;

import org.mantiscorp.effectiverate.business.Entity;

import java.util.Map;
import java.util.Properties;
import java.util.function.Supplier;

/**
 *
 */
abstract class CachedEntityParser<E extends Entity> extends EntityParser<E> {
    private final Map<Integer, E> entityCache;

    protected CachedEntityParser(Supplier<E> entityCreator, Map<Integer, E> entityCache) {
        super(entityCreator);
        this.entityCache = entityCache;
    }

    @Override
    public E apply(Properties t) {
        final E entity = super.apply(t);
        cacheEntity(entity);
        return entity;
    }

    @Override
    public void reset() {
        super.reset();
        this.entityCache.clear();
    }

    @Override
    protected void loadEntity(Properties values, E entity){
        entity.setId(getIntProperty(values, "_id").orElseThrow(IllegalStateException::new));
    }

    protected void cacheEntity(E entity) {
        cacheEntity(entityCache, entity);
    }

    protected void cacheEntity(Map<Integer, E> map, E entity) {
        map.put(entity.getId(), entity);
    }
}
