package org.mantiscorp.effectiverate.backupfile;

import org.mantiscorp.effectiverate.backupfile.entityparser.*;
import org.mantiscorp.effectiverate.business.Currency;
import org.mantiscorp.effectiverate.business.*;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.GZIPInputStream;

/**
 *
 */
public class BackupFileEntitiesParser {
    private final Map<String, EntityParser<?>> entityParsers = new HashMap<>();

    private static final String ENTITY_ACCOUNT = "account";
    private static final String ENTITY_CATEGORY = "category";
    private static final String ENTITY_CURRENCY = "currency";
    private static final String ENTITY_EXCHANGE_RATE = "currency_exchange_rate";
    private static final String ENTITY_PROJECT = "project";
    private static final String ENTITY_PAYEE = "payee";
    private static final String ENTITY_TRANSACTION = "transactions";

    private final Map<Integer, Currency> currencies = new HashMap<>();
    private final Collection<Transaction> transactions = new ArrayList<>();

    public BackupFileEntitiesParser() {
        super();

        final Map<Integer, Account> accounts = new HashMap<>();
        final Map<Integer, Category> categories = new HashMap<>();
        final Map<Integer, Project> projects = new HashMap<>();
        final Map<Integer, Payee> payees = new HashMap<>();

        entityParsers.put(ENTITY_ACCOUNT, new AccountParser(accounts, currencies));
        entityParsers.put(ENTITY_CATEGORY, new CategoryParser(categories));
        entityParsers.put(ENTITY_CURRENCY, new CurrencyParser(currencies));
        entityParsers.put(ENTITY_EXCHANGE_RATE, new ExchangeRateParser(currencies));
        entityParsers.put(ENTITY_PROJECT, new ProjectParser(projects));
        entityParsers.put(ENTITY_PAYEE, new PayeeParser(payees));
        entityParsers.put(ENTITY_TRANSACTION, new TransactionParser(transactions, accounts, categories, projects, payees));
    }

    public Collection<Transaction> parseBackupFile(String fileName) throws IOException {
        try (InputStream is = new GZIPInputStream(new FileInputStream(fileName))) {
            final BufferedReader reader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
            //noinspection StatementWithEmptyBody
            while (!"#START".equals(reader.readLine())) ; // skip header
            return parseBackupFile(reader);
        }
    }

    private Collection<Transaction> parseBackupFile(BufferedReader reader) throws IOException {
        entityParsers.values().forEach(EntityParser::reset);

        final BackupFileParser fileParser = new BackupFileParser();
        final Collection<Properties> entities = fileParser.parse(reader, ENTITY_CATEGORY, ENTITY_CURRENCY,
                ENTITY_PROJECT, ENTITY_PAYEE, ENTITY_EXCHANGE_RATE, ENTITY_ACCOUNT, ENTITY_TRANSACTION);

        parseEntities(entities, ENTITY_CATEGORY, ENTITY_CURRENCY, ENTITY_PROJECT, ENTITY_PAYEE);
        parseEntities(entities, ENTITY_EXCHANGE_RATE, ENTITY_ACCOUNT);
        parseEntities(entities, ENTITY_TRANSACTION);

        return transactions;
    }

    public Collection<Currency> getCurrencies() {
        return currencies.values();
    }

    private void parseEntities(Collection<Properties> entities, String... entityNames) {
        parseEntities(entities, Stream.of(entityNames).collect(Collectors.toSet()));
    }

    private void parseEntities(Collection<Properties> entities, Collection<String> entityNames) {
        final Iterator<Properties> it = entities.iterator();
        while (it.hasNext()) {
            final Properties entity = it.next();
            final String entityName = BackupFileParser.getEntityName(entity);
            if (entityNames.contains(entityName)) {
                final EntityParser<?> entityParser = Objects.requireNonNull(entityParsers.get(entityName));
                entityParser.apply(entity);
                it.remove();
            }
        }
    }
}
