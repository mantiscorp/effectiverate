package org.mantiscorp.effectiverate.backupfile;

import lombok.NoArgsConstructor;
import lombok.extern.java.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 */
@Log
@NoArgsConstructor
public class BackupFileParser {

    public Collection<Properties> parse(BufferedReader reader, String... entityNames) throws IOException {
        return parse(reader, Stream.of(entityNames).collect(Collectors.toSet()));
    }

    public Collection<Properties> parse(BufferedReader reader, Collection<String> entityNames) throws IOException {
        final Collection<Properties> result = new LinkedList<>();
        final Properties currentEntity = new Properties();

        String s;
        while ((s = reader.readLine()) != null) {
            switch (s) {
                case "#END":
                    return result;
                case "$$":
                    if (entityNames.contains(getEntityName(currentEntity))) {
                        final Properties entity = new Properties();
                        entity.putAll(currentEntity);
                        result.add(entity);
                    }
                    currentEntity.clear();
                    break;
                default:
                    int pos = s.indexOf(":");
                    if (pos == -1)
                        throw new IllegalStateException(s);
                    else
                        currentEntity.put(s.substring(0, pos), s.substring(pos + 1));
            }
        }
        return result;
    }

    public static String getEntityName(Properties properties) {
        return properties.getProperty("$ENTITY");
    }
}
